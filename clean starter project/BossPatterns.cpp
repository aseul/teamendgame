#include "BossPatterns.h"
#include "ObjectController.h"

bool BossBullet::IsOffscreen() const
{
    float halfWidth = 0.f;
    float halfHeight = 0.f;
    if (!IsGameLevel_BossFight)
    {
        halfWidth = 800 / 2.f;
        halfHeight = 800 / 2.f;
    }
    else
    {
        halfWidth = 800 / 2.f;
        halfHeight = 800 / 2.f;
    }
    bool isOnscreen = transform.position.x > -halfWidth &&
        transform.position.x < halfWidth &&
        transform.position.y > -halfHeight &&
        transform.position.y < halfHeight;
    return !isOnscreen;
}





void CheckbossBullet(std::list<BossBullet*> &bossbulletList, State *CurrentState,Object &Bar)
{
    BossBullet *currentbossBullet = NULL;
    std::list<BossBullet*>::iterator i = bossbulletList.begin();
    while (i != bossbulletList.end())
    {
        currentbossBullet = *i;
        if (currentbossBullet->IsOffscreen())
        {
            CurrentState->RemoveObject(currentbossBullet);
            delete currentbossBullet;
            currentbossBullet = NULL;
            bossbulletList.erase(i++);
            
        }
        else if (currentbossBullet->physics.IsCollidingWith("Player"))
        {
            Player_HP -= BOSS_BULLET_DAMAGE;
            Bar.transform.SetScale(Player_HP, 22);
            Bar.transform.position.x -= BOSS_BULLET_DAMAGE / 2;
            CurrentState->RemoveObject(currentbossBullet);
            delete currentbossBullet;
            currentbossBullet = NULL;
            bossbulletList.erase(i++);
        }
        else
        {
            i++;
        }
    }
}

BossBullet* CreatebossBullet(State *CurrentState, std::list<BossBullet*> &bossbulletList, b2Vec2 position)
{
    BossBullet *newbossBullet = new BossBullet;

    newbossBullet->SetName("BossBullet");

    newbossBullet->transform.position.Set(position.x, position.y, 0.f);
    newbossBullet->transform.SetScale(BULLET_DIAMETER, BULLET_DIAMETER);

    newbossBullet->sprite.color = Color::Brown;

    newbossBullet->sprite.LoadImage("texture/circle.png", State::m_renderer);

    newbossBullet->physics.GenerateBody(CurrentState->GetPhysicsWorld(), &newbossBullet->transform);

    CurrentState->AddObject(newbossBullet);
    bossbulletList.push_back(newbossBullet);

    return newbossBullet;
}

void Simple_Plus_Spread(State *CurrentState, std::list<BossBullet*> &bossbulletList,float speed)
{
    b2Vec2 Up = { 0,speed };
    b2Vec2 Down = { 0, -speed };
    b2Vec2 Left = { -speed, 0 };
    b2Vec2 Right = { speed, 0 };
    ShootbossBullet(Up, Up, CurrentState, bossbulletList);
    ShootbossBullet(Down, Down, CurrentState, bossbulletList);
    ShootbossBullet(Left, Left, CurrentState, bossbulletList);
    ShootbossBullet(Right, Right, CurrentState, bossbulletList);
}

void Simple_Multiple_Spread(State *CurrentState, std::list<BossBullet*> &bossbulletList, float speed)
{
    b2Vec2 Up_Right = { speed,speed };
    b2Vec2 UP_Left = { -speed, speed };
    b2Vec2 Down_Left = { -speed, -speed };
    b2Vec2 Down_Right = { speed, -speed };
    ShootbossBullet(Up_Right, Up_Right, CurrentState, bossbulletList);
    ShootbossBullet(UP_Left, UP_Left, CurrentState, bossbulletList);
    ShootbossBullet(Down_Left, Down_Left, CurrentState, bossbulletList);
    ShootbossBullet(Down_Right, Down_Right, CurrentState, bossbulletList);
}

void ShootbossBullet(b2Vec2 Location, b2Vec2 Velocity, State *CurrentState, std::list<BossBullet*> &bossbulletList)
{
    BossBullet *newBullet = CreatebossBullet(CurrentState, bossbulletList, Location);
    newBullet->physics.ActiveGhostCollision(true);
    newBullet->physics.SetVelocity(Velocity);
}

void Simple_Both_Spread(State *CurrentState, std::list<BossBullet*> &bossbulletList, float speed)
{
    b2Vec2 Up = { 0,speed };
    b2Vec2 Up_Right = { speed,speed };
    b2Vec2 Right = { speed, 0 };
    b2Vec2 Down_Right = { speed, -speed };
    b2Vec2 Down = { 0, -speed };
    b2Vec2 Down_Left = { -speed, -speed };
    b2Vec2 Left = { -speed, 0 };
    b2Vec2 UP_Left = { -speed, speed };
    ShootbossBullet(Up, Up, CurrentState, bossbulletList);
    ShootbossBullet(Up_Right, Up_Right, CurrentState, bossbulletList);
    ShootbossBullet(Right, Right, CurrentState, bossbulletList);
    ShootbossBullet(Down_Right, Down_Right, CurrentState, bossbulletList);
    ShootbossBullet(Down, Down, CurrentState, bossbulletList);
    ShootbossBullet(Down_Left, Down_Left, CurrentState, bossbulletList);
    ShootbossBullet(Left, Left, CurrentState, bossbulletList);
    ShootbossBullet(UP_Left, UP_Left, CurrentState, bossbulletList);
}

void Which_Patterns_Do(const int Patterns, State *CurrentState, std::list<BossBullet*> &bossbulletList)
{
    /*BossBullet* obj = new BossBullet();
    obj->SetName("BossBullet");

    obj->transform.position.Set(0.0f, 0.0f, 0.f);
    obj->transform.SetScale(BULLET_DIAMETER, BULLET_DIAMETER);

    obj->sprite.color = Color::Brown;

    obj->sprite.LoadImage("texture/circle.png", State::m_renderer);

    obj->physics.GenerateBody(CurrentState->GetPhysicsWorld(), &obj->transform);
    obj->physics.ActiveGhostCollision = true;
   
    CurrentState->AddObject(obj);
    bossbulletList.push_back(obj);*/
    switch (Patterns)
    {
    case(BossPatterns::Simple_Plus_Spread):
        if (Patterns_cnt == 1 || Patterns_cnt == 10 || Patterns_cnt == 25 || Patterns_cnt == 50 || Patterns_cnt == 85)
            Simple_Plus_Spread(CurrentState, bossbulletList, 30);
        break;
    case(BossPatterns::Simple_Multiple_Spread):
        if (Patterns_cnt == 1 || Patterns_cnt == 10 || Patterns_cnt == 25 || Patterns_cnt == 50 || Patterns_cnt == 85)
            Simple_Multiple_Spread(CurrentState, bossbulletList, 30);
        break;
    case(BossPatterns::Simple_Both_Spread):
        if (Patterns_cnt == 5 || Patterns_cnt == 20 || Patterns_cnt == 40 || Patterns_cnt == 60 || Patterns_cnt == 80 )
            Simple_Plus_Spread(CurrentState, bossbulletList, (float)Patterns_cnt);
        else if (Patterns_cnt == 10 || Patterns_cnt == 30 || Patterns_cnt == 50 || Patterns_cnt == 70 || Patterns_cnt == 90 )
            Simple_Multiple_Spread(CurrentState, bossbulletList, (float)Patterns_cnt);
        break;
    }
}


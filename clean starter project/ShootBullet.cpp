#include "ShootBullet.h"


Bullet* CreateBullet(b2Vec2 position, State *CurrentState, std::list<Bullet*> &bulletList)
{
    Bullet *newBullet = new Bullet;
    int i = abs(rand() % 3);
    // Provide this Object with a unique identifier (e.g. "Bullet_1", "Bullet_2", etc.)
    newBullet->SetName("Bullet");

    newBullet->transform.position.Set(position.x, position.y, 0.f);
    newBullet->transform.SetScale(BULLET_DIAMETER, BULLET_DIAMETER);

    newBullet->sprite.color = Color::WHITE;

    switch (i)
    {
    case(0):
        newBullet->sprite.LoadImage("texture/bullet.png", State::m_renderer);
        break;
    case(1):
        newBullet->sprite.LoadImage("texture/bullet_h.png", State::m_renderer);
        break;
    case(2):
        newBullet->sprite.LoadImage("texture/ZIP.png", State::m_renderer);
        break;
    }

    newBullet->physics.GenerateBody(CurrentState->GetPhysicsWorld(), &newBullet->transform);

    CurrentState->AddObject(newBullet);
    bulletList.push_back(newBullet);

    return newBullet;
}

bool Bullet::IsOffscreen() const
{
    bool IsCenterScreen = transform.position.x > -7.f && transform.position.x < 7.f && transform.position.y > -7.f && transform.position.y < 7.f;
    return IsCenterScreen;
}


b2Vec2 GetNormalizedDirectionVector(Object &Player)
{
    b2Vec2 directionVector;

    float rotationRadians = (Player.transform.rotation + 90.f) * theata;

    directionVector.x = cos(rotationRadians);

    directionVector.y = sin(rotationRadians);

    return directionVector;
}


b2Vec2 SetBulletPosition()
{
    b2Vec2 bullet_position;
    bullet_position.x = cos(theata * angle) * (radious - 40);
    bullet_position.y = sin(theata* angle) * (radious - 40);
    return bullet_position;
}


b2Vec2 GetVelocityVector(Object &Player)
{
    b2Vec2 velocityVector;
    b2Vec2 directionVector = GetNormalizedDirectionVector(Player);

    velocityVector.x = BULLET_VELOCITY * directionVector.x;
    velocityVector.y = BULLET_VELOCITY * directionVector.y;

    return velocityVector;
}

void CheckBullet(std::list<Bullet*> &bulletList,State *CurrentState)
{
    Bullet *currentBullet = NULL;
    std::list<Bullet*>::iterator i = bulletList.begin();
    while (i != bulletList.end())
    {
        currentBullet = *i;
        if (currentBullet->IsOffscreen())
        {
            CurrentState->RemoveObject(currentBullet);
            delete currentBullet;
            currentBullet = NULL;
            bulletList.erase(i++);
        }
        else
        {
            i++;
        }
    }
}

void CheckBulletBossLevel(std::list <Bullet*> &bulletList, State *CurrentState, Object&BossHP)
{
    Bullet *currentBullet = NULL;
    std::list<Bullet*>::iterator i = bulletList.begin();
    while (i != bulletList.end())
    {
        currentBullet = *i;
        if (currentBullet->IsOffscreen() || currentBullet->physics.IsCollidingWith("Boss"))
        {
            BOSS_HP += PLAYER_BULLET_DAMAGE;
            BossHP.transform.SetScale(BOSS_HP, 22);
            BossHP.transform.position.x += PLAYER_BULLET_DAMAGE / 2;

            CurrentState->RemoveObject(currentBullet);
            delete currentBullet;
            currentBullet = NULL;
            bulletList.erase(i++);
        }
        else
        {
            i++;
        }
    }
}

void ShootBullet(Object &Player,State *CurrentState, std::list<Bullet*> &bulletList)
{
    // Create bullet at the tip of the turret
    b2Vec2 Location = SetBulletPosition();
    Bullet *newBullet = CreateBullet(Location,CurrentState,bulletList);
    newBullet->physics.ActiveGhostCollision(true);
    b2Vec2 velocityVector = GetVelocityVector(Player);
    newBullet->physics.SetVelocity(velocityVector.x, velocityVector.y);
}

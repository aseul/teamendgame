#include "ObjectController.h"


void BuildAndRegisterBody(Object &body, std::string name, float positionX,
    float positionY, float Scale_X, float Scale_Y, std::string FileName, SDL_Color Colors, Physics::BodyShape shape, bool ishud, State *CurrentState)
{
    const float POSITION_Z = 0.f;
    const float RADIUS = 32.f;
    const float RESTITUTION = 1.f;

    body.SetName(name.c_str());

    body.transform.position.Set(positionX, positionY, POSITION_Z);
    body.transform.SetScale(Scale_X, Scale_Y);
    body.transform.rotation = theata;

    body.sprite.color = Colors;

    body.sprite.LoadImage(FileName.c_str(), State::m_renderer);

    body.physics.bodyType = Physics::STATIC;
    body.physics.bodyShape = shape;
    body.physics.radius = RADIUS;
    body.physics.GenerateBody(CurrentState->GetPhysicsWorld(), &body.transform);
    body.physics.SetRestitution(RESTITUTION);
    body.sprite.isHud = ishud;
    CurrentState->AddObject(&body);

    // Add to moving object list
}

void CreatePlayer(Object &Player,State *CurrentState)
{
    const float RADIUS = 32.f;
    const float RESTITUTION = 1.f;

    Player.SetName("Player");

    Player.transform.position.Set(0.f, -100.f, 0.f);
    Player.transform.SetScale(40.f, 70.f);
    Player.transform.rotation = theata;

    Player.sprite.LoadImage("texture/motion.png", State::m_renderer);
    Player.sprite.activeAnimation = true;
    Player.sprite.SetFrame(2);
    Player.sprite.SetSpeed(2.f);


    Player.physics.bodyType = Physics::STATIC;
    Player.physics.bodyShape = Physics::BOX;
    Player.physics.radius = RADIUS;
    Player.physics.GenerateBody(CurrentState->GetPhysicsWorld(), &Player.transform);
    Player.physics.SetRestitution(RESTITUTION);
    Player.sprite.isHud = false;
    CurrentState->AddObject(&Player);

}
void CreateAndRegisterHudText(Object &textObject, const char *textContents,
    float SizeX, float SizeY, float PostionX, float PostionY, SDL_Color Colors, TTF_Font* font, const char *id, State *CurrentState)
{
    // Set the object's name
    textObject.SetName(id);

    // Set the scale of the object
    textObject.transform.SetScale(SizeX, SizeY);

    // Set the Postion of the object
    textObject.transform.position.Set(PostionX, PostionY, 0);

    // Set the text to render out
    textObject.text.SetText(State::m_renderer, textContents, font);

    // Set the colors r,g,b,a (0~255)
    textObject.text.color = Colors;

    // Set either if the object to be hud or not
    textObject.text.isHud = true;

    // Register the object to the state
    CurrentState->AddObject(&textObject);
}
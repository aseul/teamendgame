#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "ShootBullet.h"
//#include "BossPatterns.h"
#include <list>

class BossBullet : public Object
{
public:
    bool IsOffscreen() const;
};

class David : public State
{
	friend class Game;
	friend class Bullet;
    friend class BossBullet;

protected:

	David() : State() {};
	~David() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	// Derived Close function
	void Close() override;

    void CheckbossBullet();
    BossBullet* CreatebossBullet(b2Vec2 position);
    void Simple_Plus_Spread(float speed);
    void Simple_Multiple_Spread(float speed);
    void ShootbossBullet(b2Vec2 Location, b2Vec2 Velocity);
    void Simple_Both_Spread(float speed);
    void Which_Patterns_Do();
	void Half_Space();


private:
    std::list<BossBullet*> bossbulletList;
	std::list<Bullet*> bulletList;
	Object title1Text;
	Object title2Text;
	Object TEAMNAME;
	Object TeamLOGO;
	Object Player;
    Object Boss;
    Object Warn_Box1;
    Object Warn_Box2;
    Object Warn_Box3;
    Object Warn_Box4;
    Object Warn_Box5;
    Object Warn_Box6;
    Object Warn_Box7;
    Object Warn_Box8;
    Object PlayerHPbar;
    Object BossHPbar;
    Object PlayerGauge;
    Object BossGauage;
	Object Circleborder[3];


};
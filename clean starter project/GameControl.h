#pragma once
namespace BossPatterns
{
    const int Simple_Plus_Spread = 0;
    const int Simple_Multiple_Spread = 1;
    const int Simple_Both_Spread = 2;
	const int Half_Space = 3;
};

extern bool IsGameLevel_BossFight;
extern bool Boss_Can_Attack;
extern float Boss_Warn_Delay;
extern int Patterns_cnt;
extern float BOSS_HP;

#pragma once
#include "CommonLevel.h"
#include <list>

class BossBullet : public Object
{
public:
    bool IsOffscreen() const;
};

BossBullet* CreatebossBullet(State *CurrentState, std::list<BossBullet*> &bossbulletList, b2Vec2 position);
void CheckbossBullet(std::list<BossBullet*> &bossbulletList, State *CurrentState, Object &Bar);
void ShootbossBullet(b2Vec2 Location, b2Vec2 Velocity, State *CurrentState, std::list<BossBullet*> &bossbulletList);

void Which_Patterns_Do(const int Patterns, State *CurrentState,std::list<BossBullet*> &bossbulletList);
void Simple_Plus_Spread(State *CurrentState,std::list<BossBullet*> &bossbulletList,float speed);
void Simple_Multiple_Spread(State *CurrentState, std::list<BossBullet*> &bossbulletList,float speed);
void Simple_Both_Spread(State *CurrentState, std::list<BossBullet*> &bossbulletList, float speed);
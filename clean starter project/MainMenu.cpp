#include "CommonLevel.h"
#include "PlayerControl.h"
#include "ObjectController.h"

void MainMenu::Initialize()
{
	IsGameLevel_BossFight = false;
	ROTATION_DELTA = 5;
	BULLET_VELOCITY = 25.f;
	BULLET_DIAMETER = 20.f;
	//initialize player contol objects
	theata = (float)M_PI / 180.0f;
	angle = 270.0f;
	radious = 100.0f;
	Player_position = 1;
	ISmoveup_restrict = true;
	ISmovedown_restrict = false;
	IsPlayer_CorrectPosition = true;
    Player_Canshoot = true;
	delay = 0.f;

	// set physics world
	b2Vec2 gravity(0.f, 0.f);
	SetPhysicsWorld(gravity);
	m_backgroundColor = Color::WHITE;


	// Set Camera
	// You don't need to do this anymore
	//camera.Initialize(int(State::m_width), int(State::m_height));
	// Set position of the camera
	camera.position.Set(0, 0, 0);
	// Set main font
	// Load the font file to use as a main font
	// and set the default size of it
    mainFont = TTF_OpenFont("font/JaykopDot.ttf", 48);
    BuildAndRegisterBody(TeamLOGO, "LOGO", 0.f, 460.f, 100.f, 100.f, "texture/logo.png", Color::BLACK, Physics::BOX, true, this);
    BuildAndRegisterBody(Circleborder[0], "Circleborder1", 0.f, 0.f, 350, 350, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    BuildAndRegisterBody(Circleborder[1], "Circleborder2", 0.f, 0.f, 560, 560, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    BuildAndRegisterBody(Circleborder[2], "Circleborder3", 0.f, 0.f, 800, 800, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    CreatePlayer(Player,this);
    BuildAndRegisterBody(GameStart, "game_start", 0.f, 350.f, 170.f, 50.f, "texture/rect.png", Color::WHITE, Physics::BOX, false, this);
    GameStart.sprite.LoadImage("texture/game_start.png", m_renderer);
    // set text position and color in the middle, game logo will be there
    CreateAndRegisterHudText(title1Text, "CAPTAIN", 200.f, 53.f, -180.f, 460.f, Color::RED, mainFont, "Text1", this);
    CreateAndRegisterHudText(title2Text, "DIGIPEN", 200.f, 53.f, 180.f, 460.f, Color::BLUE, mainFont, "Text2", this);
    CreateAndRegisterHudText(TEAMNAME, "TEAM_ENDGAME", 150.f, 22.5f, 0.f, -450.f, Color::BLACK, mainFont, "TEAMTEXT", this);
}




void MainMenu::Update(float dt)
{
	CheckBullet(bulletList, this);

	if (m_input->IsTriggered(SDL_SCANCODE_SPACE) && Player_Canshoot)
	{
		ShootBullet(Player, this, bulletList);
		Player_Canshoot = false;
	}
	if (Player_Canshoot == false)
	{
		PlayerShootDelay(dt);
	}

	if (m_input->IsPressed((SDL_SCANCODE_LEFT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_LEFT, Player);
	}
	else if (m_input->IsPressed((SDL_SCANCODE_RIGHT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_RIGHT, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_UP)) && !ISmoveup_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_UP, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_DOWN)) && !ISmovedown_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_DOWN, Player);
	}
	// Must be one of the last functions called at the end of State Update
	UpdatePlayerPosition(dt, Player);
	Render(dt);
	UpdatePhysics(dt);
	TextMoveCenter_GameStart();
	CheckCollision();


}

bool MainMenu::TextMoveCenter_GameStart()
{
	if (Player.transform.position.x > -85.f && Player.transform.position.x < 85.f)
	{
		if (Player.transform.position.y > 100.f && Player.transform.position.y < 400.f)
		{
			GameStart.transform.position = { 0,0,0 };
			return true;
		}
		else
		{
			GameStart.transform.position = { 0,350.f,0 };
			return false;
		}
	}
	else
	{
		GameStart.transform.position = { 0,350.f,0 };

		return false;
	}
}


void MainMenu::Close()
{
	// Wrap up state
	ClearBaseState();
}

void MainMenu::CheckCollision()
{
	if (Player.physics.HasBody()) {
		if (GameStart.physics.IsColliding())
		{
			if (TextMoveCenter_GameStart())
			{
				m_game->Change(LV_Difficulty);
			}
		}
	}
}
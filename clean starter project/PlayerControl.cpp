#include "PlayerControl.h"


void ControlObjects(float dt, int key_input, Object &Player)
{
    switch (key_input)
    {
    case(SDL_SCANCODE_LEFT):
        angle -= 0.15f *dt * 1000 / (float)Player_position;
        Player.transform.rotation -= 0.15f *dt * 1000 / (float)Player_position;
        break;
    case (SDL_SCANCODE_RIGHT):
        angle += 0.15f*dt * 1000 / (float)Player_position;
        Player.transform.rotation += 0.15f *dt * 1000 / (float)Player_position;
        break;
    case (SDL_SCANCODE_UP):
        if (IsPlayer_CorrectPosition)
        { 
            Player_position -= 1;
            IsPlayer_CorrectPosition = false;
        }
        break;
    case (SDL_SCANCODE_DOWN):
        if (IsPlayer_CorrectPosition)
        {
            Player_position += 1;
            IsPlayer_CorrectPosition = false;
        }
        break;
    }
}

void UpdatePlayerPosition(float dt, Object &Player)
{
    float Player_correctposition;

    if (Player_position == 1)
    {
        ISmoveup_restrict = true;
        Player_correctposition = 175.f*Player_position;

        UpdatePlayerMoving(dt, Player_correctposition);
    }
    else if (Player_position == 2)
    {
        ISmoveup_restrict = false;
        ISmovedown_restrict = false;
        Player_correctposition = 140.f*Player_position;

        UpdatePlayerMoving(dt, Player_correctposition);

    }
    else if (Player_position == 3)
    {
        ISmovedown_restrict = true;

        Player_correctposition = 133.f*Player_position;

        UpdatePlayerMoving(dt, Player_correctposition);
    }
    else
    {
        ISmovedown_restrict = false;
        ISmoveup_restrict = false;
    }


    Player.transform.position.x =
        +cos(theata * angle) * radious;

    Player.transform.position.y =
        +sin(theata* angle) * radious;

}

void UpdatePlayerMoving(float dt, float Player_correctposition)
{

    if (radious != Player_correctposition)
    {
        if (radious < Player_correctposition) // raidous < correct pos
        {
            radious += 100 * dt *3.5f;
            if (radious >= Player_correctposition)
            {
                radious = Player_correctposition;
                IsPlayer_CorrectPosition = true;
            }
        }
        else// radious > correct pos
        {
            radious -= 100 * dt *3.5f;
            if (radious <= Player_correctposition)
            {
                radious = Player_correctposition;
                IsPlayer_CorrectPosition = true;
            }
        }
    }
}

void PlayerShootDelay(float dt)
{
    delay += dt * 4.5f;
    if (delay >= 2.f)
    {
        Player_Canshoot = true;
        delay = 0;
    }
}


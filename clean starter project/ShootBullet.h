#pragma once
#include "CommonLevel.h"
#include <list>

class Bullet : public Object
{
public:
    bool IsOffscreen() const;
    //b2Vec2 BulletPosition;
};

Bullet* CreateBullet(b2Vec2 position,State *CurrentState, std::list<Bullet*> &bulletList);
b2Vec2 GetNormalizedDirectionVector(Object &Player);
b2Vec2 SetBulletPosition();
b2Vec2 GetVelocityVector(Object &Player);
void CheckBullet(std::list<Bullet*> &bulletList, State *CurrentState);
void ShootBullet(Object &Player,State *CurrentState, std::list<Bullet*> &bulletList);
void CheckBulletBossLevel(std::list <Bullet*> &bulletList, State *CurrentState, Object&BossHP);
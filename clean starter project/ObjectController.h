#pragma once
#include "CommonLevel.h"

void CreatePlayer(Object &Player, State *CurrentState);
void BuildAndRegisterBody(Object &body, std::string name, float positionX,
    float positionY, float Scale_X, float Scale_Y, std::string FileName, SDL_Color Colors, Physics::BodyShape shape, bool ishud, State *CurrentState);
void  CreateAndRegisterHudText(Object &textObject, const char *textContents,
    float SizeX, float SizeY, float PostionX, float PostionY, SDL_Color Colors, TTF_Font* font, const char *id, State *CurrentState);
#include "CommonLevel.h"
#include "PlayerControl.h"
#include "ObjectController.h"

void David::Initialize()
{
	IsGameLevel_BossFight = true;
	ROTATION_DELTA = 5;
	BULLET_VELOCITY = 25.f;
	BULLET_DIAMETER = 20.f;
	//initialize player contol objects
	angle = 270.0f;
	radious = 100.0f;
	Player_position = 1;
	ISmoveup_restrict = true;
	ISmovedown_restrict = false;
	IsPlayer_CorrectPosition = true;
	Player_Canshoot = true;
	Patterns_cnt = 0;
	delay = 0.f;


	Boss_Can_Attack = false;
	Boss_Pattern = BossPatterns::Simple_Plus_Spread;


	// you can change those values to manage the bullet damage & player HP
	// CAUTION :: when you change player_HP, you need to change sprites,
	// sprites are matched with PLAYER&BOSS_HP
	Player_HP = 296.f;
	BOSS_HP = 0.f;
	PLAYER_BULLET_DAMAGE = 40.6f;
	BOSS_BULLET_DAMAGE = 8.f;
	// set physics world

	b2Vec2 gravity(0.f, 0.f);
	SetPhysicsWorld(gravity);
	m_backgroundColor = Color::WHITE;


	camera.position.Set(0, 0, 0);

	mainFont = TTF_OpenFont("font/JaykopDot.ttf", 48);
	BuildAndRegisterBody(TeamLOGO, "LOGO", 0.f, 460.f, 100.f, 100.f, "texture/logo.png", Color::BLACK, Physics::BOX, true, this);
	BuildAndRegisterBody(Circleborder[0], "Circleborder1", 0.f, 0.f, 350, 350, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
	BuildAndRegisterBody(Circleborder[1], "Circleborder2", 0.f, 0.f, 560, 560, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
	BuildAndRegisterBody(Circleborder[2], "Circleborder3", 0.f, 0.f, 800, 800, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
	CreatePlayer(Player, this);
	BuildAndRegisterBody(Boss, "Boss", 0.f, 0.f, 80.f, 80.f, "texture/PROFESSOR_D.png", Color::WHITE, Physics::BOX, false, this);

	// set text position and color in the middle, game logo will be there

	CreateAndRegisterHudText(title1Text, "CAPTAIN", 200.f, 53.f, -180.f, 460.f, Color::RED, mainFont, "Text1", this);
	CreateAndRegisterHudText(title2Text, "DIGIPEN", 200.f, 53.f, 180.f, 460.f, Color::BLUE, mainFont, "Text2", this);
	CreateAndRegisterHudText(TEAMNAME, "TEAM_ENDGAME", 150.f, 22.5f, 0.f, -480.f, Color::BLACK, mainFont, "TEAMTEXT", this);


	BuildAndRegisterBody(Warn_Box1, "Warn1", 0.f, 285.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	BuildAndRegisterBody(Warn_Box2, "Warn2", 0.f, -285.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	BuildAndRegisterBody(Warn_Box3, "Warn3", 285.f, 0.f, 225.f, 30.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	BuildAndRegisterBody(Warn_Box4, "Warn4", -285.f, 0.f, 225.f, 30.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);

	// up right
	BuildAndRegisterBody(Warn_Box5, "Warn5", 202.f, 202.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	// up left
	BuildAndRegisterBody(Warn_Box6, "Warn6", -202.f, 202.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	// down left
	BuildAndRegisterBody(Warn_Box7, "Warn7", -202.f, -202.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);
	// down left
	BuildAndRegisterBody(Warn_Box8, "Warn8", 202.f, -202.f, 30.f, 225.f, "texture/rect.png", Color::Invisible_RED, Physics::BOX, true, this);

	Warn_Box5.transform.rotation += 135;
	Warn_Box6.transform.rotation += 45;
	Warn_Box7.transform.rotation += 135;
	Warn_Box8.transform.rotation += 45;


	BuildAndRegisterBody(BossHPbar, "BOSSHPBAR", 0.f, 425.f, 600.f, 30.f, "texture/PlayerHP.png", Color::WHITE, Physics::BOX, true, this);
	BuildAndRegisterBody(PlayerHPbar, "PLAYERHPBAR", 0.f, -425.f, Player_HP + 4, 30.f, "texture/BossHP.png", Color::WHITE, Physics::BOX, true, this);
	BuildAndRegisterBody(BossGauage, "BOSSHPGAUAGE", -296.f, 425.f, BOSS_HP, 22.f, "texture/rect.png", Color::RED, Physics::BOX, true, this);
	BuildAndRegisterBody(PlayerGauge, "PLAYERHPGAUAGE", 0.f, -425.f, Player_HP/*max is 592*/, 22.f, "texture/rect.png", Color::RED, Physics::BOX, true, this);
	camera.position.Set(0, 0, 0);
}

void David::Update(float dt)
{
	CheckbossBullet();
	CheckBulletBossLevel(bulletList, this, BossGauage);

	if (m_input->IsTriggered(SDL_SCANCODE_SPACE) && Player_Canshoot)
	{
		ShootBullet(Player, this, bulletList);
		Player_Canshoot = false;
	}
	if (Player_Canshoot == false)
	{
		PlayerShootDelay(dt);
	}

	if (m_input->IsPressed((SDL_SCANCODE_LEFT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_LEFT, Player);
	}
	else if (m_input->IsPressed((SDL_SCANCODE_RIGHT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_RIGHT, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_UP)) && !ISmoveup_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_UP, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_DOWN)) && !ISmovedown_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_DOWN, Player);
	}

	if (!Boss_Can_Attack)
	{
		Boss_Warn_Delay += dt;
		if (Boss_Pattern == BossPatterns::Simple_Plus_Spread)
		{
			if (Boss_Warn_Delay < 2.f&&Boss_Warn_Delay > 1.f)
			{
				Warn_Box1.sprite.color = { 255, 50, 50, 100 };
				Warn_Box2.sprite.color = { 255, 50, 50, 100 };
				Warn_Box3.sprite.color = { 255, 50, 50, 100 };
				Warn_Box4.sprite.color = { 255, 50, 50, 100 };
			}
			if (Boss_Warn_Delay > 2.f)
			{
				Boss_Can_Attack = true;
				Boss_Warn_Delay = 0.f;
				Warn_Box1.sprite.color = { 255, 50, 50, 0 };
				Warn_Box2.sprite.color = { 255, 50, 50, 0 };
				Warn_Box3.sprite.color = { 255, 50, 50, 0 };
				Warn_Box4.sprite.color = { 255, 50, 50, 0 };
			}
		}
		else if (Boss_Pattern == BossPatterns::Simple_Multiple_Spread)
		{
			if (Boss_Warn_Delay < 2.f&&Boss_Warn_Delay > 1.f)
			{
				Warn_Box5.sprite.color = { 255, 50, 50, 100 };
				Warn_Box6.sprite.color = { 255, 50, 50, 100 };
				Warn_Box7.sprite.color = { 255, 50, 50, 100 };
				Warn_Box8.sprite.color = { 255, 50, 50, 100 };
			}
			if (Boss_Warn_Delay > 2.f)
			{
				Boss_Can_Attack = true;
				Boss_Warn_Delay = 0.f;
				Warn_Box5.sprite.color = { 255, 50, 50, 0 };
				Warn_Box6.sprite.color = { 255, 50, 50, 0 };
				Warn_Box7.sprite.color = { 255, 50, 50, 0 };
				Warn_Box8.sprite.color = { 255, 50, 50, 0 };
			}
		}
		else if (Boss_Pattern == BossPatterns::Simple_Both_Spread)
		{
			if (Boss_Warn_Delay < 2.f&&Boss_Warn_Delay > 1.f)
			{
				Warn_Box1.sprite.color = { 255, 50, 50, 100 };
				Warn_Box2.sprite.color = { 255, 50, 50, 100 };
				Warn_Box3.sprite.color = { 255, 50, 50, 100 };
				Warn_Box4.sprite.color = { 255, 50, 50, 100 };
				Warn_Box5.sprite.color = { 255, 50, 50, 100 };
				Warn_Box6.sprite.color = { 255, 50, 50, 100 };
				Warn_Box7.sprite.color = { 255, 50, 50, 100 };
				Warn_Box8.sprite.color = { 255, 50, 50, 100 };
			}
			if (Boss_Warn_Delay > 2.f)
			{
				Boss_Can_Attack = true;
				Boss_Warn_Delay = 0.f;
				Warn_Box1.sprite.color = { 255, 50, 50, 0 };
				Warn_Box2.sprite.color = { 255, 50, 50, 0 };
				Warn_Box3.sprite.color = { 255, 50, 50, 0 };
				Warn_Box4.sprite.color = { 255, 50, 50, 0 };
				Warn_Box5.sprite.color = { 255, 50, 50, 0 };
				Warn_Box6.sprite.color = { 255, 50, 50, 0 };
				Warn_Box7.sprite.color = { 255, 50, 50, 0 };
				Warn_Box8.sprite.color = { 255, 50, 50, 0 };
			}
		}
		else if (Boss_Pattern == BossPatterns::Half_Space)
		{
			if (Boss_Warn_Delay < 2.f&&Boss_Warn_Delay > 1.f)
			{
				Warn_Box8.sprite.color = { 255, 50, 50, 100 };
			}
			if (Boss_Warn_Delay > 2.f)
			{
				Boss_Can_Attack = true;
				Boss_Warn_Delay = 0.f;
				Warn_Box8.sprite.color = { 255, 50, 50, 0 };
			}
		}
	}

	if (Boss_Can_Attack)
	{
		Patterns_cnt += 1;
		Which_Patterns_Do();
		if (Patterns_cnt == 100)
		{
			Patterns_cnt = 0;
			Boss_Can_Attack = false;
			Boss_Pattern = abs(rand() % 4);
			std::cout << Boss_Pattern;
		}
	}


	// Must be one of the last functions called at the end of State Update
	UpdatePlayerPosition(dt, Player);
	UpdatePhysics(dt);
	if (BOSS_HP >= 592.f || Player_HP <= 0.f)
	{
		m_game->Change(LV_Grading);
	}
	Render(dt);
	std::cout << "the size is " << GetObjects().size() << std::endl;
}



void David::Which_Patterns_Do()
{

	float BossHP_Percent = (BOSS_HP / 592.f) * 100.0f;

	switch (Boss_Pattern)
	{
	case(BossPatterns::Simple_Plus_Spread):
		if (Patterns_cnt == 1 || Patterns_cnt == 10 || Patterns_cnt == 25 || Patterns_cnt == 50 || Patterns_cnt == 85)
			Simple_Plus_Spread(30);
		break;
	case(BossPatterns::Simple_Multiple_Spread):
		if (Patterns_cnt == 1 || Patterns_cnt == 10 || Patterns_cnt == 25 || Patterns_cnt == 50 || Patterns_cnt == 85)
			Simple_Multiple_Spread(30);
		break;
	case(BossPatterns::Simple_Both_Spread):
		if (Patterns_cnt == 5 || Patterns_cnt == 20 || Patterns_cnt == 40 || Patterns_cnt == 60 || Patterns_cnt == 80)
			Simple_Plus_Spread((float)Patterns_cnt);
		else if (Patterns_cnt == 10 || Patterns_cnt == 30 || Patterns_cnt == 50 || Patterns_cnt == 70 || Patterns_cnt == 90)
			Simple_Multiple_Spread((float)Patterns_cnt);
	case(BossPatterns::Half_Space):
		if (BossHP_Percent > 75.0f)
		{
			if (Patterns_cnt == 1 || Patterns_cnt == 10 || Patterns_cnt == 25 || Patterns_cnt == 50 || Patterns_cnt == 85)
			{
				Half_Space();
			}
		}
		break;
	}
}


void David::CheckbossBullet()
{
	BossBullet *currentbossBullet = NULL;
	std::list<BossBullet*>::iterator i = bossbulletList.begin();
	while (i != bossbulletList.end())
	{
		currentbossBullet = *i;
		if (currentbossBullet->IsOffscreen())
		{
			RemoveObject(currentbossBullet);
			delete currentbossBullet;
			currentbossBullet = NULL;
			bossbulletList.erase(i++);

		}
		else if (currentbossBullet->physics.IsCollidingWith("Player"))
		{

			Player_HP -= BOSS_BULLET_DAMAGE;
			PlayerGauge.transform.SetScale(Player_HP, 22);
			PlayerGauge.transform.position.x -= BOSS_BULLET_DAMAGE / 2;
			RemoveObject(currentbossBullet);
			delete currentbossBullet;
			currentbossBullet = NULL;
			bossbulletList.erase(i++);
		}
		else
		{
			i++;
		}
	}
}

BossBullet* David::CreatebossBullet(b2Vec2 position)
{
	BossBullet *newbossBullet = new BossBullet;

	newbossBullet->SetName("BossBullet");

	newbossBullet->transform.position.Set(position.x, position.y, 0.f);
	newbossBullet->transform.SetScale(BULLET_DIAMETER, BULLET_DIAMETER);

	newbossBullet->sprite.color = Color::Brown;

	newbossBullet->sprite.LoadImage("texture/circle.png", m_renderer);

	newbossBullet->physics.GenerateBody(GetPhysicsWorld(), &newbossBullet->transform);

	AddObject(newbossBullet);
	bossbulletList.push_back(newbossBullet);

	return newbossBullet;
}

void David::Simple_Plus_Spread(float speed)
{
	b2Vec2 Up = { 0,speed };
	b2Vec2 Down = { 0, -speed };
	b2Vec2 Left = { -speed, 0 };
	b2Vec2 Right = { speed, 0 };
	ShootbossBullet(Up, Up);
	ShootbossBullet(Down, Down);
	ShootbossBullet(Left, Left);
	ShootbossBullet(Right, Right);
}

void David::Simple_Multiple_Spread(float speed)
{
	b2Vec2 Up_Right = { speed,speed };
	b2Vec2 UP_Left = { -speed, speed };
	b2Vec2 Down_Left = { -speed, -speed };
	b2Vec2 Down_Right = { speed, -speed };
	ShootbossBullet(Up_Right, Up_Right);
	ShootbossBullet(UP_Left, UP_Left);

	ShootbossBullet(Down_Left, Down_Left);
	ShootbossBullet(Down_Right, Down_Right);
}

void David::ShootbossBullet(b2Vec2 Location, b2Vec2 Velocity)
{
	BossBullet *newBullet = CreatebossBullet(Location);
	newBullet->physics.ActiveGhostCollision(true);
	newBullet->physics.SetVelocity(Velocity);
}

void David::Simple_Both_Spread(float speed)
{
	b2Vec2 Up = { 0,speed };
	b2Vec2 Up_Right = { speed,speed };
	b2Vec2 Right = { speed, 0 };
	b2Vec2 Down_Right = { speed, -speed };
	b2Vec2 Down = { 0, -speed };
	b2Vec2 Down_Left = { -speed, -speed };
	b2Vec2 Left = { -speed, 0 };
	b2Vec2 UP_Left = { -speed, speed };
	ShootbossBullet(Up, Up);
	ShootbossBullet(Up_Right, Up_Right);
	ShootbossBullet(Right, Right);
	ShootbossBullet(Down_Right, Down_Right);
	ShootbossBullet(Down, Down);
	ShootbossBullet(Down_Left, Down_Left);
	ShootbossBullet(Left, Left);
	ShootbossBullet(UP_Left, UP_Left);
}

void::David::Half_Space()
{
	if (Player.transform.position.y >= 0 && Player.transform.position.y <= 400)
	{
		Player_HP = 0;
		PlayerGauge.transform.position.x = 0;
	}
}

bool BossBullet::IsOffscreen() const
{
	float halfWidth = 0.f;
	float halfHeight = 0.f;
	if (!IsGameLevel_BossFight)
	{
		halfWidth = 800 / 2.f;
		halfHeight = 800 / 2.f;
	}
	else
	{
		halfWidth = 800 / 2.f;
		halfHeight = 800 / 2.f;
	}
	bool isOnscreen = transform.position.x > -halfWidth &&
		transform.position.x < halfWidth &&
		transform.position.y > -halfHeight &&
		transform.position.y < halfHeight;
	return !isOnscreen;
}

void David::Close()
{
	ClearBaseState();
}
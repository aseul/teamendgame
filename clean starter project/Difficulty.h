/*
\file   Difficulty.h
\author Lee Wonyong
\par    email: night98200@gmail.com
\par    GAM150 demo
\par	v0
\date   2019/04/18

Set difficulty level
*/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "ShootBullet.h"
#include <list>


class Difficulty: public State
{
    friend class Game;
    friend class Bullet;

protected:

    Difficulty() : State() {};
    ~Difficulty() override {};

    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    
	bool TextMoveCenter_Freshman();
	bool TextMoveCenter_Sophmore();

	void CheckCollision();
	// Derived Close function
    void Close() override;

private:
    std::list<Bullet*> bulletList;
    Object title1Text;
    Object title2Text;
    Object TEAMNAME;
    Object TeamLOGO;
    Object Player;
    Object Freshman;
    Object Sophmore;
    Object Circleborder[3];

};
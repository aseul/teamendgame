#pragma once
#include "SDL2/SDL.h"
namespace Color
{
    const SDL_Color WHITE = { 255, 255, 255, 255 };
    const SDL_Color BLACK = { 0 , 0 , 0 ,255 };
    const SDL_Color RED = { 255, 50, 50, 255 };
    const SDL_Color BLUE = { 50, 50, 255, 255 };
    const SDL_Color Brown = { 128,0,0,255 };
    const SDL_Color Invisible_RED = { 255, 50, 50, 0 };
}
#include "Bullet_contents.h"

int ROTATION_DELTA = 5;
float BULLET_VELOCITY = 25.f;
float BULLET_DIAMETER = 30.f;
float PLAYER_BULLET_DAMAGE = 10.f;
float BOSS_BULLET_DAMAGE = 5.f;
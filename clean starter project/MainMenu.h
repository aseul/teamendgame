/******************************************************************************/
/*!
\file   MainMenu.h
\author David Ly and Juyong Jeong
\par    email: dly\@digipen.edu
\par    GAM150 demo
\par	v0
\date   2018/03/11

Conaining examples how to use engine
*/
/******************************************************************************/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "ShootBullet.h"
#include <list>


class MainMenu : public State
{
	friend class Game;
    friend class Bullet;

protected:

	MainMenu() : State() {};
	~MainMenu() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;
	//menu to Center
	bool TextMoveCenter_GameStart();
	// Derived Close function
	void Close() override;


private:

    std::list<Bullet*> bulletList;
	Object title1Text;
    Object title2Text;
	Object TEAMNAME;
    Object TeamLOGO;
    Object Player;
    Object GameStart;
    Object Circleborder[3];
    void CheckCollision();

};


#include "CommonLevel.h"
#include "PlayerControl.h"
#include "ObjectController.h"

void BossSelect::Initialize()
{
	IsGameLevel_BossFight = false;
	ROTATION_DELTA = 5;
	BULLET_VELOCITY = 25.f;
	BULLET_DIAMETER = 20.f;
	//initialize player contol objects
	theata = (float)M_PI / 180.0f;
	angle = 270.0f;
	radious = 100.0f;
	Player_position = 1;
	ISmoveup_restrict = true;
	ISmovedown_restrict = false;
	IsPlayer_CorrectPosition = true;
	Player_Canshoot = true;
	delay = 0.f;

	// set physics world
	b2Vec2 gravity(0.f, 0.f);
	SetPhysicsWorld(gravity);
    m_backgroundColor = Color::WHITE;
    mainFont = TTF_OpenFont("font/JaykopDot.ttf", 48);
    BuildAndRegisterBody(TeamLOGO, "LOGO", 0.f, 460.f, 100.f, 100.f, "texture/logo.png", Color::BLACK, Physics::BOX, true, this);
    BuildAndRegisterBody(Circleborder[0], "Circleborder1", 0.f, 0.f, 350, 350, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    BuildAndRegisterBody(Circleborder[1], "Circleborder2", 0.f, 0.f, 560, 560, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    BuildAndRegisterBody(Circleborder[2], "Circleborder3", 0.f, 0.f, 800, 800, "texture/circleborder.png", Color::WHITE, Physics::CIRCLE, false, this);
    CreatePlayer(Player, this);
    BuildAndRegisterBody(David, "David", 0.f, 170.f, 170.f, 50.f, "texture/david.png", Color::WHITE, Physics::BOX, false, this);
    BuildAndRegisterBody(Rudy, "Rudy", -300.f, 70.f, 170.f, 50.f, "texture/rudy.png", Color::WHITE, Physics::BOX, false, this);
    BuildAndRegisterBody(Kevin, "Kevin", 300.f, 70.f, 170.f, 50.f, "texture/kevin.png", Color::WHITE, Physics::BOX, false, this);


    // set text position and color in the middle, game logo will be there

    CreateAndRegisterHudText(title1Text, "CAPTAIN", 200.f, 53.f, -180.f, 460.f, Color::RED, mainFont, "Text1", this);
    CreateAndRegisterHudText(title2Text, "DIGIPEN", 200.f, 53.f, 180.f, 460.f, Color::BLUE, mainFont, "Text2", this);
    CreateAndRegisterHudText(TEAMNAME, "TEAM_ENDGAME", 150.f, 22.5f, 0.f, -450.f, Color::BLACK, mainFont, "TEAMTEXT", this);

	camera.position.Set(0, 0, 0);
}

void BossSelect::Update(float dt)
{
	CheckBullet(bulletList, this);

	if (m_input->IsTriggered(SDL_SCANCODE_SPACE) && Player_Canshoot)
	{
		ShootBullet(Player, this, bulletList);
		Player_Canshoot = false;
	}
	if (Player_Canshoot == false)
	{
		PlayerShootDelay(dt);
	}

	if (m_input->IsPressed((SDL_SCANCODE_LEFT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_LEFT, Player);
	}
	else if (m_input->IsPressed((SDL_SCANCODE_RIGHT)) && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_RIGHT, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_UP)) && !ISmoveup_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_UP, Player);
	}
	else if (m_input->IsTriggered((SDL_SCANCODE_DOWN)) && !ISmovedown_restrict  && IsPlayer_CorrectPosition)
	{
		ControlObjects(dt, SDL_SCANCODE_DOWN, Player);
	}
	// Must be one of the last functions called at the end of State Update
	UpdatePlayerPosition(dt, Player);
	Render(dt);
	UpdatePhysics(dt);
	TextMoveCenter_David();
	TextMoveCenter_Rudy();
	TextMoveCenter_Kevin();
	CheckCollision();

}

bool BossSelect::TextMoveCenter_David()
{
	if (Player.transform.position.x > -75.f && Player.transform.position.x < 75.f)
	{
		if (Player.transform.position.y > 100.f && Player.transform.position.y < 400.f)
		{
			David.transform.position = { 0,0,0 };

			return true;
		}
		else
		{
			David.transform.position = { 0,350.f,0 };

			return false;
		}
	}
	else
	{
		David.transform.position = { 0,350.f,0 };

		return false;
	}
}

bool BossSelect::TextMoveCenter_Rudy()
{
	if (Player.transform.position.x < -120.f && Player.transform.position.x > -400.f)
	{
		if (Player.transform.position.y > 0.f && Player.transform.position.y < 120.f)
		{
			Rudy.transform.position = { 0,0,0 };
			return true;
		}
		else
		{
			Rudy.transform.position = { -300.f, 70.f, 0};
			return false;
		}
	}
	else
	{
		Rudy.transform.position = { -300.f, 70.f, 0 };

		return false;
	}
}

bool BossSelect::TextMoveCenter_Kevin()
{
	if (Player.transform.position.x > 120.f && Player.transform.position.x < 400.f)
	{
		if (Player.transform.position.y > 0.f && Player.transform.position.y < 120.f)
		{
			Kevin.transform.position = { 0,0,0 };

			return true;
		}
		else
		{
			Kevin.transform.position = { 300.f, 70.f, 0 };

			return false;
		}
	}
	else
	{
		Kevin.transform.position = { 300.f, 70.f, 0 };

		return false;
	}

}


void BossSelect::Close()
{
	ClearBaseState();
}


void BossSelect::CheckCollision()
{
	if (Player.physics.HasBody()) {
		if (David.physics.IsColliding())
		{
			if (TextMoveCenter_David())
			{
				m_game->Change(LV_David);
			}
		}
	}
}
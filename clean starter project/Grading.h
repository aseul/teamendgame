#pragma once
#include "engine\State.h"
#include "engine\Object.h"


class Grading : public State
{
    friend class Game;

protected:

    Grading() : State() {};
    ~Grading() override {};
    
    // Derived initialize function
    void Initialize() override;
    // Derived Update function
    void Update(float dt) override;
    // Derived Close function
    void Close() override;

private:
    Object grading;

};
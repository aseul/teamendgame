#include "CommonLevel.h"
#include "ObjectController.h"

// Derived initialize function
void Grading::Initialize() 
{
    IsGameLevel_BossFight = true;
    camera.position.Set(0, 0, 0);
    m_backgroundColor = Color::BLACK;
    BuildAndRegisterBody(grading, "GradingScore", 0.f, 0.f, 800.f, 1024.f, "texture/F.png", Color::WHITE, Physics::BOX, true, this);
}
// Derived Update function
void Grading::Update(float dt)
{
    float Player_Point_Percent = (Player_HP / 296.f *100.f);
    if (Player_Point_Percent >= 99.f)
    {
        grading.sprite.LoadImage("texture/A+.png",m_renderer);
    }
    else if (Player_Point_Percent >= 90.f)
    {
        grading.sprite.LoadImage("texture/A.png", m_renderer);
    }
    else if (Player_Point_Percent >= 70.f)
    {
        grading.sprite.LoadImage("texture/B+.png", m_renderer);
    }
    else if (Player_Point_Percent >= 40.f)
    {
        grading.sprite.LoadImage("texture/B.png", m_renderer);
    }
    else if (Player_Point_Percent >= 20.f)
    {
        grading.sprite.LoadImage("texture/C+.png", m_renderer);
    }
    else if (Player_Point_Percent > 0.f)
    {
        grading.sprite.LoadImage("texture/C+.png", m_renderer);
    }

    if (m_input->IsTriggered(SDL_SCANCODE_F))
    {
        m_game->Change(LV_MainMenu);
    }

    Render(dt);
}
// Derived Close function
void Grading::Close()
{
    ClearBaseState();
}
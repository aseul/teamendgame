/*
\file   BossSelect.h
\author Chung Hanseul
\par    email: chuaseul@gmail.com
\par    GAM150 demo
\par	v0
\date   2019/05/18

Select Boss
*/
#pragma once
#include "engine\State.h"
#include "engine\Object.h"
#include "ShootBullet.h"
#include <list>


class BossSelect : public State
{
	friend class Game;
	friend class Bullet;

protected:

    BossSelect() : State() {};
	~BossSelect() override {};

	// Derived initialize function
	void Initialize() override;
	// Derived Update function
	void Update(float dt) override;

	bool TextMoveCenter_David();
	bool TextMoveCenter_Rudy();
	bool TextMoveCenter_Kevin();

	void CheckCollision();
	// Derived Close function
	void Close() override;

private:
	std::list<Bullet*> bulletList;
	Object title1Text;
	Object title2Text;
	Object TEAMNAME;
	Object TeamLOGO;
	Object Player;
	Object David;
	Object Rudy;
	Object Kevin;
	Object Circleborder[3];


};